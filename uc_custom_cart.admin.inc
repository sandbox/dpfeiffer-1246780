<?php
function uc_custom_cart_admin_form($form, &$form_state) {
  drupal_set_title('UC Custom Cart Settings');

  // initializing some variables
  $form = array();
  $column = array();
  $col_default = array();
  $col_default[1]['title'] = 'Remove';
  $col_default[2]['title'] = 'Product';
  $col_default[3]['title'] = '';
  $col_default[4]['title'] = 'Qty';
  $col_default[5]['title'] = 'Total';
  $col_default[1]['key'] = 'remove';
  $col_default[2]['key'] = 'image';
  $col_default[3]['key'] = 'desc';
  $col_default[4]['key'] = 'qty';
  $col_default[5]['key'] = 'total';
  // grab total number of columns
  $num_cols = variable_get('uc_custom_cart_num_cols',5);
  $cols_extra = variable_get('uc_custom_cart_column_extra',1);

  $form['column_numbers'] = array(
    '#title' => t('Number of Columns'),
    '#type' => 'select',
    //'#options' => array('5' => '5', '6' => '6', '7' => '7', '8' => '8'),
    '#options' => array('5' => '5', '6' => '6'),
    '#default_value' => $num_cols,
    '#description' => t('Default is 5'),
  );

  $form['column_extra'] = array(
    '#title' => t('Style of Extra Column (only handles 1 extra for now)'),
    '#type' => 'select',
    '#options' => array('1' => 'Sell price - single item', '2' => 'List price - per item', '3' => 'List price - total', '4' => 'Savings - total'),
    '#default_value' => $cols_extra,
  );
  
    
  $form['column_headers'] = array(
    '#title' => t('Column Headers'),
    '#type' => 'fieldset',
    '#description' => t('Set custom values for your shopping cart table headers'),
  );
  
  // how many columns are we using?
  for($i = 1; $i <= $num_cols; $i++) {
  
    //set name defaults for 1-5 else fall back to numbered columns for 6+
    if (isset($col_default[$i])) {
      $default_col_key = $col_default[$i]['key'];
      $default_col_title = $col_default[$i]['title'];
    } else {
      $default_col_key = 'col_'.$i;
      $default_col_title = 'Column '.$i.' Title';      
    }
    $column[$i]['title'] = variable_get('uc_custom_cart_col_title_'.$i,$default_col_title);
    $column[$i]['key'] = variable_get('uc_custom_cart_col_key_'.$i,$default_col_key);
    $column[$i]['class'] = variable_get('uc_custom_cart_col_class_'.$i,'cart-header-'.$i);
    
    // setting the titles
    $form['column_headers']['col_title_'.$i] = array(
    '#title' => t('Column '.$i.' Title'),
    '#type' => 'textfield',
    '#default_value' => t($column[$i]['title']),
    );
    
    // setting the keys (this is hidden)
    $form['column_headers']['col_key_'.$i] = array(
    '#title' => t('Column '.$i.' Key'),
    '#type' => 'hidden',
    '#default_value' => t($column[$i]['key']),
    );
    
    // setting the classes
    $form['column_headers']['col_class_'.$i] = array(
    '#title' => t('Column '.$i.' Class'),
    '#type' => 'textfield',
    '#default_value' => t($column[$i]['class']),
    );
  }

  $form['save_configuration'] = array(
    '#type'=> 'submit',
    '#value' => t('Save Configuration'),
  );

  return $form;
}

/**
 * Submit the main admin form
 */
function uc_custom_cart_admin_form_submit($form, &$form_state) {
  drupal_set_message(t('Configuration Saved'));
  foreach($form_state['values'] as $key => $val) {
    if(strstr($key, 'col_')) { //only want 'Column' data
      variable_set('uc_custom_cart_'.$key, $val);
    } elseif($key == 'column_numbers') {
      variable_set('uc_custom_cart_num_cols', $val);
    } elseif($key == 'column_extra') {
      variable_set('uc_custom_cart_column_extra', $val);
    }
    
    
  }

}